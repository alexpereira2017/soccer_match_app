package com.example.soccermatch.model;

public class Api {
    protected String Rota;

    public Api(String rota, String token) {
        Rota = rota;
        Token = token;
    }

    public String getRota() {
        return Rota;
    }

    public void setRota(String rota) {
        Rota = rota;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    protected String Token;
}
