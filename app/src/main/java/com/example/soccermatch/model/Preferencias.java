package com.example.soccermatch.model;

public class Preferencias {
    private int _id;
    private String nome;
    private String valor;

    public Preferencias() {}

    public Preferencias(int _id, String nome, String valor) {
        this._id = _id;
        this.nome = nome;
        this.valor = valor;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
