package com.example.soccermatch.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.soccermatch.R;
import com.example.soccermatch.model.Preferencias;
import com.example.soccermatch.repository.PreferenciasRepository;

import java.util.List;
import java.util.Objects;

public class PreferenciasActivity extends AppCompatActivity {
    protected PreferenciasRepository repo;
    protected Preferencias novaPreferencia;
    protected Preferencias preferencia;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferencias);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        repo = new PreferenciasRepository(this);

        preferencia = repo.getPreferencias("cor");

        if (!Objects.isNull(preferencia.getNome())) {
            TextView t = findViewById(R.id.valuePreferenciaCor);
            t.setText(preferencia.getValor());
        }

        Button btSalvar = findViewById(R.id.bt_salvar_preferencias);
        btSalvar.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {

                TextView tx = findViewById(R.id.valuePreferenciaCor);
                preferencia.setValor(tx.getText().toString());

                if (Objects.isNull(preferencia.getNome())) {
                    repo.insert(preferencia);
                } else {
                    repo.update(preferencia);
                }
            }
        });
    }

}
