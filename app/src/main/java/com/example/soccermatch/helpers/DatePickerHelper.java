package com.example.soccermatch.helpers;

import android.app.DatePickerDialog;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.soccermatch.activity.AgendamentoNovoActivity;

import java.util.Calendar;

public class DatePickerHelper {
    public void run(View button, final TextView inputAlvo) {
        Calendar calendar = Calendar.getInstance();
        int dia = calendar.get(Calendar.DAY_OF_MONTH);
        int mes = calendar.get(Calendar.MONTH);
        int ano = calendar.get(Calendar.YEAR);
        DatePickerDialog datePicker = new DatePickerDialog(button.getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int ano, int mes, int dia) {
                        inputAlvo.setText(dia+"/"+(mes +1)+"/"+ano);
                    }
                }, ano, mes, dia);
        datePicker.show();
    }
}
