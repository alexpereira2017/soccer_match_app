package com.example.soccermatch.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.example.soccermatch.R;
import com.example.soccermatch.model.AgendaAtleta;
import com.example.soccermatch.model.AgendaJogos;
import com.example.soccermatch.services.AgendaService;
import com.example.soccermatch.services.AtletaService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AgendaAtletaAdapter  extends RecyclerView.Adapter<AgendaAtletaAdapter.MyViewHolder> {
    List<AgendaAtleta> listaAgendaAtleta = new ArrayList<>();
    Context contexto;

    public AgendaAtletaAdapter(List<AgendaAtleta> listaAgendaAtleta, Context contexto) {
        this.listaAgendaAtleta = listaAgendaAtleta;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemList = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.adapter_lista_agenda_atletas, viewGroup, false);
        return new MyViewHolder(itemList);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(@NonNull final AgendaAtletaAdapter.MyViewHolder myViewHolder, int i) {
        AgendaAtleta agenda = listaAgendaAtleta.get(i);
//        myViewHolder.atleta.setText(agenda.getAtleta());
        myViewHolder.time.setText(agenda.getTime());
        myViewHolder.hora.setText(agenda.getHora());
        myViewHolder.dia.setText(agenda.getDia());
//        myViewHolder.dataConfirmacao.setText(agenda.getDataConfirmacao());
        myViewHolder.local.setText(agenda.getLocal());
        myViewHolder.setIdJogo(agenda.getIdJogo());
        myViewHolder.setIdTimeAtleta(agenda.getIdTimeAtleta());
        Log.d("AgendaAtletaAdapter", "onBindViewHolder");
        Boolean resultado = true;
        if (Objects.isNull(agenda.getDataConfirmacao())) {
            resultado = false;
        }
        myViewHolder.checkboxConfirmacao.setChecked(resultado);
        myViewHolder.checkboxConfirmacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("AgendaAtletaAdapter",
                        "\nCod Atleta: "+myViewHolder.idTimeAtleta+" " +
                            "\nCod Jogo: "+myViewHolder.idJogo +
                            "\nCheckbox marcado? : "+myViewHolder.checkboxConfirmacao.isChecked());

                AtletaService service = new AtletaService();
                service.marcarPresenca(
                        myViewHolder.idTimeAtleta,
                        myViewHolder.idJogo,
                        myViewHolder.checkboxConfirmacao.isChecked());
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaAgendaAtleta.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView atleta;
        TextView time;
        TextView hora;
        TextView dia;
        TextView dataConsulta;
        TextView dataConfirmacao;
        TextView local;
        CheckBox checkboxConfirmacao;
        String idJogo;

        String idTimeAtleta;

        public String getIdJogo() {
            return idJogo;
        }

        public String getIdTimeAtleta() {
            return idTimeAtleta;
        }

        public void setIdTimeAtleta(String idTimeAtleta) {
            this.idTimeAtleta = idTimeAtleta;
        }

        public void setIdJogo(String idJogo) {
            this.idJogo = idJogo;
        }

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            time = itemView.findViewById(R.id.time);
            hora = itemView.findViewById(R.id.hora);
            dia = itemView.findViewById(R.id.dia);
//            dataConfirmacao = itemView.findViewById(R.id.dataConfirmacao);
            local = itemView.findViewById(R.id.local);
            checkboxConfirmacao = itemView.findViewById(R.id.checkboxConfirmacao);
        }
    }
}
