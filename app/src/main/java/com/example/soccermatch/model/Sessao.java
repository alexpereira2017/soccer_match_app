package com.example.soccermatch.model;

import java.io.Serializable;

public class Sessao implements Serializable {
    protected String bgApp;
    protected Integer idUsuario;

    public Sessao() {}

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Sessao(String bgApp) {
        this.bgApp = bgApp;
    }

    public String getBgApp() {
        return bgApp;
    }

    public void setBgApp(String bgApp) {
        this.bgApp = bgApp;
    }
}
