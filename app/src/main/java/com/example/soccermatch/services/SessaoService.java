package com.example.soccermatch.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.soccermatch.api.ApiData;
import com.example.soccermatch.config.Constantes;
import com.example.soccermatch.model.AgendaAtleta;
import com.example.soccermatch.model.Preferencias;
import com.example.soccermatch.model.Sessao;
import com.example.soccermatch.model.Usuario;
import com.example.soccermatch.model.response.RetornoAgendaAtleta;
import com.example.soccermatch.model.response.RetornoUsuario;
import com.example.soccermatch.repository.PreferenciasRepository;
import com.example.soccermatch.util.ApiServiceFactory;
import com.google.firebase.auth.FirebaseUser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import retrofit2.Call;

public class SessaoService {

    private ApiData apiService;
    protected Sessao sessao;
    protected Context context;
    private SharedPreferences sharedpreferences;
    private Map<String, ?> all;
    private List<Usuario> listaUsuario = new ArrayList<>();
    private RetornoUsuario retornoApi;
    protected FirebaseUser firebaseUser = null;

    public FirebaseUser getFirebaseUser() {
        return firebaseUser;
    }

    public void setFirebaseUser(FirebaseUser firebaseUser) {
        this.firebaseUser = firebaseUser;
    }

    public SessaoService(Context context) {
        this.context = context;
        apiService = ApiServiceFactory.create();
    }

    public void montar() {
        final List<Usuario> usuarios = pesquisarUsuario(firebaseUser.getEmail());
        setSharedPreferences(usuarios);
    }

    public void montar(String email) {
        final List<Usuario> usuarios = pesquisarUsuario(email);
        setSharedPreferences(usuarios);
    }

    private void setSharedPreferences(List<Usuario> usuarios) {
        sharedpreferences = context.getSharedPreferences(
                Constantes.MINHAS_PREFERENCIAS, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (usuarios != null) {
            editor.putString("NomeUsuario", usuarios.get(0).getNomeUsuario());
            editor.putString("Email", usuarios.get(0).getEmail());
            editor.putString("NomeAtleta", usuarios.get(0).getNomeAtleta());
            editor.putString("IdTimeAtleta", usuarios.get(0).getIdTimeAtleta());
            editor.putString("TimePrincipal", usuarios.get(0).getTime());
            editor.putString("IdTime", "1");
        } else {
            editor.putString("NomeUsuario", "System Demo");
            editor.putString("Email", "demo@gmail.com");
            editor.putString("NomeAtleta", "Demo");
            editor.putString("IdTimeAtleta", "3");
            editor.putString("TimePrincipal", "Bravo");
            editor.putString("IdTime", "1");
        }
        editor.apply();
        editor.commit();
    }

    public List<Usuario> pesquisarUsuario(String email) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        Call<RetornoUsuario> call = apiService.pesquisarUsuario(email);
        try {
            retornoApi = call.execute().body();
            listaUsuario = retornoApi.getDados();

        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d("pesquisarUsuario >", "Deve retornar uma lista de usuarios");
        return listaUsuario;
    }

    private void getAll() {
        SharedPreferences prefs = context.getSharedPreferences(
                Constantes.MINHAS_PREFERENCIAS, Context.MODE_PRIVATE);
        all = prefs.getAll();
    }

    public String getByName(String key) {
        /*
        *  SessaoService sessaoService = new SessaoService(this);
        *  sessaoService.getByName("Email");
        * */
        getAll();
        if (all.containsKey(key)) {
            return (String) all.get(key);
        }
        return "";
    }

    public Sessao getSessao() {
        return sessao;
    }
}
