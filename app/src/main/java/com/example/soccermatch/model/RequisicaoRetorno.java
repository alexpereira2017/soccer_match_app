package com.example.soccermatch.model;

public interface RequisicaoRetorno {
    public String status = null;
    public String mensagem = null;
    public String getStatus();

    public void setStatus(String status);

    public String getMensagem();

    public void setMensagem(String mensagem);
}
