package com.example.soccermatch.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TimeSorteado extends Api {
    private String Nome;
    private String Setor;
    private String Posicao;
    private String IdPosicao;
    private String Avaliacao;
    private String Time;

    public TimeSorteado(String rota, String token, String nome, String setor, String posicao, String idPosicao, String avaliacao, String time) {
        super(rota, token);
        Nome = nome;
        Setor = setor;
        Posicao = posicao;
        IdPosicao = idPosicao;
        Avaliacao = avaliacao;
        Time = time;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getSetor() {
        return Setor;
    }

    public void setSetor(String setor) {
        Setor = setor;
    }

    public String getPosicao() {
        return Posicao;
    }

    public void setPosicao(String posicao) {
        Posicao = posicao;
    }

    public String getIdPosicao() {
        return IdPosicao;
    }

    public void setIdPosicao(String idPosicao) {
        IdPosicao = idPosicao;
    }

    public String getAvaliacao() {
        return Avaliacao;
    }

    public void setAvaliacao(String avaliacao) {
        Avaliacao = avaliacao;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }
}

