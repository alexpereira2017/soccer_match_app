package com.example.soccermatch.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.soccermatch.R;
import com.example.soccermatch.model.AgendaJogos;

import java.util.ArrayList;
import java.util.List;

public class AgendaAdapter extends RecyclerView.Adapter<AgendaAdapter.MyViewHolder> {
    List<AgendaJogos> listaAgendaJogos = new ArrayList<>();

    public AgendaAdapter(List<AgendaJogos> agendaJogos) {
        this.listaAgendaJogos = agendaJogos;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View itemList = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.adapter_lista_agendamento, viewGroup, false);
        return new MyViewHolder(itemList);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        AgendaJogos agenda = listaAgendaJogos.get(i);
        myViewHolder.local.setText(agenda.getLocal());
        myViewHolder.dia.setText(agenda.getDia());
        myViewHolder.hora.setText(agenda.getHora());
    }

    @Override
    public int getItemCount() {
        return listaAgendaJogos.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView local;
        TextView hora;
        TextView dia;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            local = itemView.findViewById(R.id.adapter_agendamento_local);
            hora = itemView.findViewById(R.id.adapter_agendamento_hora);
            dia = itemView.findViewById(R.id.adapter_agendamento_dia);
        }
    }

}
