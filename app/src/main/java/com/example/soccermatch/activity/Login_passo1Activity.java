package com.example.soccermatch.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.soccermatch.R;

public class Login_passo1Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_passo1);

        Button btNovaConta = findViewById(R.id.bt_criarConta);
        btNovaConta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intencao = new Intent(getApplicationContext(), Login_criarContaActivity.class);
                startActivity(intencao);
            }
        });

        Button btEntrar = findViewById(R.id.bt_novoUsuario);
        btEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intencao = new Intent(getApplicationContext(), Login_passo2Activity.class);
                startActivity(intencao);
            }
        });
    }
}
