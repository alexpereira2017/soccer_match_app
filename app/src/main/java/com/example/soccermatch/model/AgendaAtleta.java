package com.example.soccermatch.model;

public class AgendaAtleta extends Api {
    private String atleta;
    private String time;
    private String hora;
    private String idJogo;
    private String idTimeAtleta;
    private String dia;
    private String dataConsulta;
    private String dataConfirmacao;
    private String local;

    public AgendaAtleta(String rota, String token) {
        super(rota, token);
    }

    public String getIdTimeAtleta() {
        return idTimeAtleta;
    }

    public void setIdTimeAtleta(String idTimeAtleta) {
        this.idTimeAtleta = idTimeAtleta;
    }
    public String getIdJogo() {
        return idJogo;
    }

    public void setIdJogo(String idJogo) {
        this.idJogo = idJogo;
    }

    public String getAtleta() {
        return atleta;
    }

    public void setAtleta(String atleta) {
        this.atleta = atleta;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getDataConsulta() {
        return dataConsulta;
    }

    public void setDataConsulta(String dataConsulta) {
        this.dataConsulta = dataConsulta;
    }

    public String getDataConfirmacao() {
        return dataConfirmacao;
    }

    public void setDataConfirmacao(String dataConfirmacao) {
        this.dataConfirmacao = dataConfirmacao;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }
}
