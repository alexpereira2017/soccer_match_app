package com.example.soccermatch.model.response;

import com.example.soccermatch.model.AgendaAtleta;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RetornoAgendaAtleta {
    private String status;
    private String mensagem;

    @SerializedName("dados")
    private List<AgendaAtleta> dados;

    public RetornoAgendaAtleta(String status, String mensagem, List<AgendaAtleta> dados) {
        this.status = status;
        this.mensagem = mensagem;
        this.dados = dados;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public List<AgendaAtleta> getDados() {
        return dados;
    }

    public void setDados(List<AgendaAtleta> dados) {
        this.dados = dados;
    }
}
