package com.example.soccermatch.activity;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.soccermatch.R;
import com.example.soccermatch.adapter.AgendaAtletaAdapter;
import com.example.soccermatch.adapter.SorteadosAdapter;
import com.example.soccermatch.model.AgendaAtleta;
import com.example.soccermatch.model.AgendaJogos;
import com.example.soccermatch.model.TimeSorteado;
import com.example.soccermatch.services.AtletaService;
import com.example.soccermatch.services.SessaoService;
import com.example.soccermatch.services.TimesService;

import java.util.Arrays;
import java.util.List;

public class SortearActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private Spinner campoAgenda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sortear);
        campoAgenda = findViewById(R.id.spinnerJogosFuturos);
        carregarDadosSpinner();
        controlarBotaoSortear();
    }

    private void controlarBotaoSortear() {
        Button btSortear = findViewById(R.id.bt_sortear);
        btSortear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SessaoService sessaoService = new SessaoService(getApplicationContext());
                String IdTime = sessaoService.getByName("IdTime");

                Spinner mySpinner = (Spinner) findViewById(R.id.spinnerJogosFuturos);
                String dataEscolhida = mySpinner.getSelectedItem().toString();

                TimesService timesApi = new TimesService();
                final List<TimeSorteado> sorteados = timesApi.sortear(
                        Integer.parseInt(IdTime),
                        dataEscolhida);


                SorteadosAdapter adapter = new SorteadosAdapter(sorteados, getApplicationContext());

                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerViewTimesSorteados);
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                recyclerView.setHasFixedSize(true);
                recyclerView.setAdapter(adapter);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
            }
        });
    }

    private void carregarDadosSpinner() {

        SessaoService sessao = new SessaoService(this);
        final String idTime = sessao.getByName("IdTime");

        TimesService timesApi = new TimesService();
        List<AgendaJogos> agenda = timesApi.buscarPartidasFuturas(Integer.parseInt(idTime));

        String[] dias = new String[agenda.size()];
        int i = 0;
        for(AgendaJogos item : agenda){
            dias[i++] = item.getDia();
        }

        Spinner spinner = (Spinner) findViewById(R.id.spinnerJogosFuturos);
        spinner.setOnItemSelectedListener(this);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, dias);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
