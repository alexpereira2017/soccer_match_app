package com.example.soccermatch.model.response;

import com.example.soccermatch.model.AgendaJogos;
import com.example.soccermatch.model.RequisicaoRetorno;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RetornoAgendaJogos implements RequisicaoRetorno {
    private String status;
    private String mensagem;

    @SerializedName("dados")
    private List<AgendaJogos> dados;

    public RetornoAgendaJogos(String status, String mensagem, List<AgendaJogos> dados) {
        this.status = status;
        this.mensagem = mensagem;
        this.dados = dados;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public List<AgendaJogos> getDados() {
        return dados;
    }

    public void setDados(List<AgendaJogos> dados) {
        this.dados = dados;
    }

    @Override
    public String toString() {
        return "RetornoAgendaJogos{" +
                "status='" + status + '\'' +
                ", mensagem='" + mensagem + '\'' +
                ", dados=" + dados.toString() +
                '}';
    }
}
