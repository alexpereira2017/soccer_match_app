package com.example.soccermatch.model.response;

import com.example.soccermatch.model.AgendaAtleta;
import com.example.soccermatch.model.Usuario;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RetornoUsuario {
    private String status;
    private String mensagem;

    @SerializedName("dados")
    private List<Usuario> dados;

    public RetornoUsuario() {
    }

    public RetornoUsuario(String status, String mensagem, List<Usuario> dados) {
        this.status = status;
        this.mensagem = mensagem;
        this.dados = dados;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public List<Usuario> getDados() {
        return dados;
    }

    public void setDados(List<Usuario> dados) {
        this.dados = dados;
    }
}

