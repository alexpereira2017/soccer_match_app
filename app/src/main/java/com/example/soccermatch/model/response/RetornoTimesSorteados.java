package com.example.soccermatch.model.response;

import com.example.soccermatch.model.AgendaAtleta;
import com.example.soccermatch.model.AgendaJogos;
import com.example.soccermatch.model.RequisicaoRetorno;
import com.example.soccermatch.model.TimeSorteado;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RetornoTimesSorteados implements RequisicaoRetorno {
    private String status;
    private String mensagem;

    @SerializedName("dados")
    private List<TimeSorteado> dados;

    public RetornoTimesSorteados(String status, String mensagem, List<TimeSorteado> dados) {
        this.status = status;
        this.mensagem = mensagem;
        this.dados = dados;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public List<TimeSorteado> getDados() {
        return dados;
    }

    public void setDados(List<TimeSorteado> dados) {
        this.dados = dados;
    }

}
