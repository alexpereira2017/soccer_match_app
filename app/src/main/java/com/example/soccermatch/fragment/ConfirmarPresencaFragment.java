package com.example.soccermatch.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.soccermatch.R;
import com.example.soccermatch.adapter.AgendaAtletaAdapter;
import com.example.soccermatch.model.AgendaAtleta;
import com.example.soccermatch.services.AtletaService;
import com.example.soccermatch.services.SessaoService;

import java.util.List;

public class ConfirmarPresencaFragment extends Fragment {
    View view;

    public ConfirmarPresencaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_confirmar_presenca, container, false);
        montar();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("ConfirmarPresenca", "Chamada realizada onDestroyView");
    }



    public void montar() {
        SessaoService sessaoService = new SessaoService(getContext());
        String IdTimeAtleta = sessaoService.getByName("IdTimeAtleta");

        AtletaService service = new AtletaService();
        List<AgendaAtleta> agenda = service.buscarPartidasFuturas(Integer.parseInt(IdTimeAtleta));
        AgendaAtletaAdapter adapter = new AgendaAtletaAdapter(agenda, getContext());
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.listaPresenca);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext().getApplicationContext()));
//        Qualquer um destes modos funciona
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
//        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getBaseContext()));
//        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

}
