package com.example.soccermatch.api;

import com.example.soccermatch.model.AgendaJogos;
import com.example.soccermatch.model.Cep;
import com.example.soccermatch.model.Presenca;
import com.example.soccermatch.model.response.RetornoAgendaAtleta;
import com.example.soccermatch.model.response.RetornoAgendaJogos;
import com.example.soccermatch.model.response.RetornoPost;
import com.example.soccermatch.model.response.RetornoTimesSorteados;
import com.example.soccermatch.model.response.RetornoUsuario;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiData {

    @GET("servicos.php?Rota=BuscarTodosTimes")
    Call<RetornoAgendaJogos> buscarTodosTimes(@Query("IdTime") int id);

    @GET("servicos.php?Rota=BuscarTimes")
    Call<RetornoAgendaJogos> buscarTimes(@Query("IdTime") int id);

    @GET("servicos.php?Rota=SortearTimesEquilibrados")
    Call<RetornoTimesSorteados> sortearTimesEquilibrados(@Query("id_time") int id, @Query("data") String data);

    @GET("servicos.php?Rota=BuscarTodosJogosDoTime")
    Call<RetornoAgendaJogos> buscarTodosJogosDoTime(@Query("IdTime") int id);

    @GET("servicos.php?Rota=BuscarPartidasFuturas")
    Call<RetornoAgendaJogos> buscarPartidasFuturas(@Query("IdTime") int id);

    @GET("servicos.php?Rota=BuscarPartidasFuturasAtleta")
    Call<RetornoAgendaAtleta> buscarPartidasFuturasAtleta(@Query("IdAtleta") int id);

    @GET("servicos.php?Rota=BuscarPartidasFuturasTimeAtleta")
    Call<RetornoAgendaAtleta> buscarPartidasFuturasTimeAtleta(@Query("IdTimeAtleta") int id);

    @GET("servicos.php?Rota=BuscarTodosAtletas")
    Call<RetornoAgendaJogos> buscarTodosAtletas(@Query("IdTime") int id);

    @GET("servicos.php?Rota=BuscarTodosAtletasDoTime")
    Call<RetornoAgendaJogos> buscarTodosAtletasDoTime(@Query("IdTime") int id);

    @GET("servicos.php?Rota=BuscarCaracteristicas")
    Call<RetornoAgendaJogos> buscarCaracteristicas(@Query("IdTime") int id);

    @GET("servicos.php?Rota=BuscarTodasAvaliacoesDoTime")
    Call<RetornoAgendaJogos> buscarTodasAvaliacoesDoTime(@Query("IdTime") int id);

    @GET("servicos.php?Rota=BuscarPosicoesJogadores")
    Call<RetornoAgendaJogos> buscarPosicoesJogadores(@Query("IdTime") int id);

    @GET("servicos.php?Rota=BuscarHistoricoDePartidasDoAtleta")
    Call<RetornoAgendaJogos> buscarHistoricoDePartidasDoAtleta(@Query("IdTime") int id);

    @GET("servicos.php?Rota=BuscarConfirmadosParaPartidaDoTimeNoDiaEHora")
    Call<RetornoAgendaJogos> buscarConfirmadosParaPartidaDoTimeNoDiaEHora(@Query("IdTime") int id);

    @GET("servicos.php?Rota=PesquisarUsuario")
    Call<RetornoUsuario> pesquisarUsuario(@Query("Email") String email);

    @GET("90820001/json/")
    Call<Cep> teste();

    @GET("{cep}/json/")
    Call<Cep> testeComParametros(@Path("cep") String cep);

    @POST("servicos.php")
    Call<RetornoAgendaJogos> salvarAgenda(@Body AgendaJogos agendaJogos);

    @POST("servicos.php")
    Call<RetornoPost> confirmarPresenca(@Body Presenca presenca);

//    @DELETE("servicos.php")
//    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "servicos.php", hasBody = true)
    Call<RetornoPost> desmarcarPresenca(@Body Presenca presenca);
}
