package com.example.soccermatch.model.response;

import com.example.soccermatch.model.AgendaJogos;
import com.example.soccermatch.model.RequisicaoRetorno;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RetornoPost implements RequisicaoRetorno {
    private String status;
    private String mensagem;

    @SerializedName("dados")
    private List<AgendaJogos> dados;

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String s) {
        status = s;
    }

    @Override
    public String getMensagem() {
        return mensagem;
    }

    @Override
    public void setMensagem(String m) {
        mensagem = m;
    }
}
