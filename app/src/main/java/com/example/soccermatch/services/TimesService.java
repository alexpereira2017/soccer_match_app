package com.example.soccermatch.services;

import android.os.StrictMode;
import android.util.Log;

import com.example.soccermatch.api.ApiData;
import com.example.soccermatch.model.AgendaJogos;
import com.example.soccermatch.model.TimeSorteado;
import com.example.soccermatch.model.response.RetornoAgendaJogos;
import com.example.soccermatch.model.response.RetornoTimesSorteados;
import com.example.soccermatch.util.ApiServiceFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class TimesService {
    private ApiData apiService;
    private List<AgendaJogos> listaAgendaJogos = new ArrayList<>();
    private List<TimeSorteado> listaTimeSorteado = new ArrayList<>();
    private RetornoAgendaJogos retornoApi;
    private RetornoTimesSorteados retornoApiTimesSorteados;

    public TimesService() {
        apiService = ApiServiceFactory.create();
    }

    public List<AgendaJogos> buscarTodosJogosDoTime(int idTime) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        Call<RetornoAgendaJogos> call = apiService.buscarTodosJogosDoTime(idTime);
        try {
            retornoApi = call.execute().body();
            listaAgendaJogos = retornoApi.getDados();

        } catch (IOException e) {
            e.printStackTrace();
        }
        // Por buscar os dados de forma assincrona, a lista retornava vazia antes da requisição ser
//        retornada. Para resolver, tive que trocar para uma chamada SINCRONA
//        call.enqueue(new Callback<List<AgendaJogos>>() {
//            @Override
//            public void onResponse(Call<List<AgendaJogos>> call, Response<List<AgendaJogos>> response) {
//                if (response.isSuccessful()) {
//                    listaAgendaJogos.clear();
//                    listaAgendaJogos.addAll(response.body());
//                    Log.d("Teste>", "Conteudo "+listaAgendaJogos.size());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<AgendaJogos>> call, Throwable t) {
//                Log.d("Teste>", "Falhou :(");
//            }
//        });
        Log.d("Teste>", "Conteudo fora "+listaAgendaJogos.size());
        return listaAgendaJogos;
    }


    public List<AgendaJogos> buscarPartidasFuturas(int idTime) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        Call<RetornoAgendaJogos> call = apiService.buscarPartidasFuturas(idTime);
        try {
            retornoApi = call.execute().body();
            listaAgendaJogos = retornoApi.getDados();

        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d("buscarPartidasFuturas>", "Conteudo fora "+listaAgendaJogos.size());
        return listaAgendaJogos;
    }

    public List<TimeSorteado> sortear(int IdTime, String dataEscolhida) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        Call<RetornoTimesSorteados> call = apiService.sortearTimesEquilibrados(IdTime, dataEscolhida);
        try {
            retornoApiTimesSorteados = call.execute().body();
            listaTimeSorteado = retornoApiTimesSorteados.getDados();

        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d("sortear>", "Conteudo fora ");
        return listaTimeSorteado;
    }
}
