package com.example.soccermatch.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.soccermatch.R;
import com.example.soccermatch.model.Usuario;
import com.example.soccermatch.services.FirebaseService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Login_passo2Activity extends AppCompatActivity {
    FirebaseService firebase = new FirebaseService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_passo2);
        Toast.makeText(getApplicationContext(), "Acessou", Toast.LENGTH_LONG).show();

        Button btEntrar = findViewById(R.id.bt_novoUsuario);
        btEntrar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "Clicou", Toast.LENGTH_LONG).show();
                TextView email = findViewById(R.id.username);
                TextView password = findViewById(R.id.password);

                Usuario usuario = new Usuario();
                usuario.setEmail(email.getText().toString());
                usuario.setSenha(password.getText().toString());

                firebase.realizarLogin(getApplicationContext(), usuario);

           }
        });
    }


}
