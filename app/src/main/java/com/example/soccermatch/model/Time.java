package com.example.soccermatch.model;

public class Time {
    private String nome;
    private String setor;
    private String posicao;
    private String id_posicao;
    private String avaliacao;

    public Time() {}

    public Time(String nome, String setor, String posicao, String id_posicao, String avaliacao) {
        this.nome = nome;
        this.setor = setor;
        this.posicao = posicao;
        this.id_posicao = id_posicao;
        this.avaliacao = avaliacao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public String getPosicao() {
        return posicao;
    }

    public void setPosicao(String posicao) {
        this.posicao = posicao;
    }

    public String getId_posicao() {
        return id_posicao;
    }

    public void setId_posicao(String id_posicao) {
        this.id_posicao = id_posicao;
    }

    public String getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(String avaliacao) {
        this.avaliacao = avaliacao;
    }
}
