package com.example.soccermatch.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.soccermatch.R;
import com.example.soccermatch.config.Constantes;
import com.example.soccermatch.config.SqliteConfig;
import com.example.soccermatch.fragment.AvaliarFragment;
import com.example.soccermatch.fragment.ConfirmarPresencaFragment;
import com.example.soccermatch.fragment.EstatisticasFragment;
import com.example.soccermatch.fragment.SobreFragment;
import com.example.soccermatch.model.Preferencias;
import com.example.soccermatch.model.Sessao;
import com.example.soccermatch.repository.PreferenciasRepository;
import com.example.soccermatch.services.FirebaseService;
import com.example.soccermatch.services.SessaoService;

import java.util.Map;
import java.util.Objects;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
{
    protected Sessao sessao;
    private BottomNavigationView bottomNavigationView;
    private Fragment fragmento;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        fragmento = new ConfirmarPresencaFragment();
        chamarFragmento(fragmento);

        Log.d("App exec", "App sendo executado");
//        interfaceBancoDeDados banco = BancoDeDadosFactory.GET("Sqlite", this);
        SqliteConfig banco = new SqliteConfig(this);
        Log.d("App exec", "App sendo executado 2");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

    }


    private void setupBottomNavigationView() {
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_confirmar_presenca: {
//                        intencao.putExtra("Sessao", sessao);
                        fragmento = new ConfirmarPresencaFragment();
                        chamarFragmento(fragmento);
                        break;
                    }
                    case R.id.navigation_estatisticas: {
                        fragmento = new EstatisticasFragment();
                        chamarFragmento(fragmento);
                        break;
                    }
                    case R.id.navigation_avaliar: {
                        fragmento = new AvaliarFragment();
                        chamarFragmento(fragmento);
                        break;
                    }
                }

                return true;
            }
        });
    }

    // @RequiresApi(api = Build.VERSION_CODES.N)
    protected void chamarFragmento(Fragment frag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.FrameLayoutHome, frag);
        transaction.addToBackStack(null);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.commit();
    }


    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.N)
    protected void onResume () {
        super.onResume();
        SessaoService sessaoService = new SessaoService(this);
        sessaoService.montar("seco@gmail.com");
        String e = sessaoService.getByName("Email");

//        sessao = sessaoService.getSessao();

        setupBottomNavigationView();

//        if (!sessao.getBgApp().isEmpty()) {
//            if (Color.isSrgb(Color.parseColor(sessao.getBgApp()))) {
//                View view = findViewById(R.id.bg_home2);
//                view.setBackgroundColor(Color.parseColor(sessao.getBgApp()));
//            }
//        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intencao = new Intent(getApplicationContext(), PreferenciasActivity.class);
            startActivity(intencao);
            return true;
        } else if (id == R.id.action_about) {
            FragmentManager fm = getSupportFragmentManager();
            DialogFragment dialogFragment = new SobreFragment();
            dialogFragment.show(fm, "tag");
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        /* Menu lateral */
        switch (item.getItemId()) {

            case R.id.nav_agendar: {
                Intent intencao = new Intent(getApplicationContext(), AgendamentoActivity.class);
                startActivity(intencao);
                break;
            }

            case R.id.nav_sortear: {
                Intent intencao = new Intent(getApplicationContext(), SortearActivity.class);
                startActivity(intencao);
                break;
            }

            case R.id.nav_sair: {
                FirebaseService fb = new FirebaseService(getApplicationContext());
                fb.encerrar();
            }
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
