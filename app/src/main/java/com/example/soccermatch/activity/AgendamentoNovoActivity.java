package com.example.soccermatch.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.soccermatch.R;
import com.example.soccermatch.helpers.DatePickerHelper;
import com.example.soccermatch.helpers.TimePickerHelper;
import com.example.soccermatch.model.RequisicaoRetorno;
import com.example.soccermatch.services.AgendaService;

import java.util.Calendar;

public class AgendamentoNovoActivity extends AppCompatActivity {
    TextView inputData;
    TextView inputHora;
    RequisicaoRetorno retornoStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agendamento_novo);

//        TextView textViewAgendamento = findViewById(R.id.bt_dashboard_agendamento);
//        textViewAgendamento.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intencao = new Intent(getApplicationContext(), AgendamentoActivity.class);
//                startActivity(intencao);
//            }
//        });

        inputData = findViewById(R.id.inputDia);
        inputHora = findViewById(R.id.inputHora);

        inputData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerHelper dp = new DatePickerHelper();
                dp.run(v, inputData);
            }
        });

        inputHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerHelper picktime = new TimePickerHelper();
                picktime.run(v, inputHora);
            }
        });

        Button btSalvar = findViewById(R.id.bt_agendamento_novo_salvar);
        btSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AgendaService service = new AgendaService();
                retornoStatus = service.novo(
                        inputData.getText().toString(),
                        inputHora.getText().toString()
                );
//                Toast.makeText(getApplicationContext(),retornoStatus.getMensagem() ,Toast.LENGTH_SHORT).show();
            }
        });
    }

}
