package com.example.soccermatch.services;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.soccermatch.activity.HomeActivity;
import com.example.soccermatch.activity.Login_passo1Activity;
import com.example.soccermatch.model.Usuario;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class FirebaseService {
    private Context context;
    private FirebaseAuth mAuth;
    protected FirebaseUser user = null;
    protected Boolean resultado;
   // private Context context;

    public FirebaseService(final Context context) {
        this.context = context;
        mAuth = FirebaseAuth.getInstance();
    }

    public FirebaseService() {
        mAuth = FirebaseAuth.getInstance();
        resultado = false;
    }

    public boolean realizarLogin(final Context context, Usuario usuario) {
        //this.context = context;
        mAuth.signInWithEmailAndPassword(usuario.getEmail(), usuario.getSenha())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d("Login com sucesso", "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            SessaoService sessaoService = new SessaoService(context);
                            sessaoService.setFirebaseUser(user);
                            sessaoService.montar();

                            Intent intencao = new Intent(context, HomeActivity.class);
                            intencao.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intencao);
                        } else {
                            Log.w("Falha no Login", "signInWithEmail:failure", task.getException());
                        }
                    }
                });
        return resultado;
    }



    private void openMainWindow(){
//        Intent intencao = new Intent(Login_passo2Activity.this, HomeActivity.class);
//        startActivity(intencao);
    }

    public boolean criarUsuarioELogar(final Context context, final Usuario usuario) {
        mAuth.createUserWithEmailAndPassword(usuario.getEmail().trim(), usuario.getSenha().trim())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d("Criacao do user", "createUserWithEmail:success");
                            user = mAuth.getCurrentUser();
                            realizarLogin(context, usuario);
                        } else {
                            Log.w("Falha user", "createUserWithEmail:failure", task.getException());
                            Log.w("Nome de usuário", usuario.getNome());
                            Log.w("Email informado", usuario.getEmail());
                        }
                    }
                });
        return resultado;
    }

    public void encerrar() {
        mAuth.signOut();
        Intent intencao = new Intent(context, Login_passo1Activity.class);
        intencao.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intencao);
    }
}
