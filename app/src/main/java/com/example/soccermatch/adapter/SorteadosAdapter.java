package com.example.soccermatch.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.soccermatch.R;
import com.example.soccermatch.model.TimeSorteado;

import java.util.ArrayList;
import java.util.List;

public class SorteadosAdapter extends RecyclerView.Adapter<SorteadosAdapter.MyViewHolder> {
    List<TimeSorteado> listaDados = new ArrayList<>();

    public SorteadosAdapter(List<TimeSorteado> listaDados, Context applicationContext) {
        this.listaDados = listaDados;
    }

    @NonNull
    @Override
    public SorteadosAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemList = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.adapter_lista_sorteados, viewGroup, false);
        return new SorteadosAdapter.MyViewHolder(itemList);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        TimeSorteado itemLista = listaDados.get(i);
        myViewHolder.nome.setText(itemLista.getNome());
        myViewHolder.time.setText(itemLista.getTime());
    }

    @Override
    public int getItemCount() {
        return listaDados.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nome;
        TextView time;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            nome = itemView.findViewById(R.id.adapter_sorteado_nome);
            time = itemView.findViewById(R.id.adapter_sorteado_time);
        }
    }
}
