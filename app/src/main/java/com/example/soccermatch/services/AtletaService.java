package com.example.soccermatch.services;

import android.os.StrictMode;
import android.util.Log;

import com.example.soccermatch.api.ApiData;
import com.example.soccermatch.model.AgendaAtleta;
import com.example.soccermatch.model.AgendaJogos;
import com.example.soccermatch.model.Presenca;
import com.example.soccermatch.model.response.RetornoAgendaAtleta;
import com.example.soccermatch.model.response.RetornoAgendaJogos;
import com.example.soccermatch.model.response.RetornoPost;
import com.example.soccermatch.util.ApiServiceFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AtletaService {
    private ApiData apiService;
    private List<AgendaAtleta> listaAgendaAtleta = new ArrayList<>();
    private RetornoAgendaAtleta retornoApi;

    public AtletaService() {
        apiService = ApiServiceFactory.create();
    }

    public List<AgendaAtleta> buscarPartidasFuturas(int idTimeAtleta) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        Call<RetornoAgendaAtleta> call = apiService.buscarPartidasFuturasTimeAtleta(idTimeAtleta);
        try {
            retornoApi = call.execute().body();
            listaAgendaAtleta = retornoApi.getDados();

        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d("buscarPartidasFuturas>", "Quantidade de itens encontrados em buscarPartidasFuturas: "+listaAgendaAtleta.size());
        return listaAgendaAtleta;
    }

    public void marcarPresenca(String idTimeAtleta, String idJogo, boolean status ) {

        Log.d("AtletaService", "Vai salvar presença");
        if (status) {
            confirmarPresenca(idTimeAtleta, idJogo);
        } else {
            cancelarPresenca(idTimeAtleta, idJogo);
        }
    }

    private void confirmarPresenca(String idTimeAtleta, String idJogo) {
        Presenca presenca = new Presenca("ConfirmarPresenca",
                "dW5pcXVlaGFzaF9BbGV4IEwgUGVyZWlyYXwxfGFsZXhwZXJlaXJhMjAwNEBnbWFpbC5jb21lbV8xNTU3OTgyODI5"
                , idTimeAtleta, idJogo);

        Call<RetornoPost> call = apiService.confirmarPresenca(presenca);

        call.enqueue(new Callback<RetornoPost>() {
            @Override
            public void onResponse(Call<RetornoPost> call, Response<RetornoPost> response) {

                if (response.isSuccessful()) {
                    Log.i("onResponse", "requisição com sucesso");
                    response.body();
                }

            }
            @Override
            public void onFailure(Call<RetornoPost> call, Throwable t) {
                Log.i("onResponse", "requisição com erro");
            }
        });
    }

    private void cancelarPresenca(String idTimeAtleta, String idJogo) {
        Presenca presenca = new Presenca("DesmarcarPresenca",
                "dW5pcXVlaGFzaF9BbGV4IEwgUGVyZWlyYXwxfGFsZXhwZXJlaXJhMjAwNEBnbWFpbC5jb21lbV8xNTU3OTgyODI5"
                , idTimeAtleta, idJogo);

        Call<RetornoPost> call = apiService.desmarcarPresenca(presenca);

        call.enqueue(new Callback<RetornoPost>() {
            @Override
            public void onResponse(Call<RetornoPost> call, Response<RetornoPost> response) {

                if (response.isSuccessful()) {
                    Log.i("onResponse", "requisição com sucesso");
                    response.body();
                }

            }
            @Override
            public void onFailure(Call<RetornoPost> call, Throwable t) {
                Log.i("onResponse", "requisição com erro");
            }
        });
    }
}
