package com.example.soccermatch.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.example.soccermatch.R;
import com.example.soccermatch.adapter.AgendaAdapter;
import com.example.soccermatch.api.ApiData;
import com.example.soccermatch.model.Sessao;
import com.example.soccermatch.services.TimesService;
import com.example.soccermatch.model.AgendaJogos;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Retrofit;

public class AgendamentoActivity extends AppCompatActivity {

    private Retrofit retrofit;
    private List<AgendaJogos> listagemAgendasJogos = new ArrayList<>();
    private ApiData apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agendamento);

//        PreferenciasRepository repo = new PreferenciasRepository(this);
//        Preferencias preferencia = repo.getPreferencias("cor");
        Intent intent = getIntent();
        Sessao sessao = (Sessao) intent.getSerializableExtra("Sessao");
        View view = findViewById(R.id.bg_agendamento);
//        view.setBackgroundColor(Color.parseColor(sessao.getBgApp()));

        Button btNovo = findViewById(R.id.bt_nova_agenda);
        btNovo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent intencao = new Intent(getApplicationContext(), AgendamentoNovoActivity.class);
            startActivity(intencao);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        TimesService timesApi = new TimesService();
        List<AgendaJogos> agenda = timesApi.buscarPartidasFuturas(1);

        RecyclerView.Adapter adapter = new AgendaAdapter(agenda);

        RecyclerView recyclerView = findViewById(R.id.listaPresenca);

//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

}
