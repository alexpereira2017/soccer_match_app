package com.example.soccermatch.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.BoringLayout;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.soccermatch.R;
import com.example.soccermatch.model.Usuario;
import com.example.soccermatch.services.FirebaseService;

public class Login_criarContaActivity extends AppCompatActivity {
    FirebaseService sessao = new FirebaseService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_criar_conta);

        Button btEntrar = findViewById(R.id.bt_novoUsuario);
        btEntrar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TextView nome = findViewById(R.id.nome);
                TextView email = findViewById(R.id.username);
                TextView password = findViewById(R.id.password);

                Usuario usuario = new Usuario();
                usuario.setNome(nome.getText().toString());
                usuario.setEmail(email.getText().toString());
                usuario.setSenha(password.getText().toString());

                Boolean ret = sessao.criarUsuarioELogar(getApplicationContext(), usuario);

                if (ret) {
                    Intent intencao = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(intencao);
                }

            }
        });
    }
}
