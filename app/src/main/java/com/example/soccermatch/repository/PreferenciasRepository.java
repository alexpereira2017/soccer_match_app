package com.example.soccermatch.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.example.soccermatch.model.Preferencias;
import com.example.soccermatch.util.BDUtil;

import java.util.ArrayList;
import java.util.List;


    public class PreferenciasRepository {
        private BDUtil bdUtil;

        public PreferenciasRepository(Context context){
            bdUtil =  new BDUtil(context);
        }

        public String insert(Preferencias preferencia){
            ContentValues valores = new ContentValues();
            valores.put("NOME", preferencia.getNome());
            valores.put("VALOR", preferencia.getValor());
            long resultado = bdUtil.getConexao().insert("PREFERENCIAS", null, valores);
            if (resultado ==-1) {
                bdUtil.close();
                return "Erro ao inserir registro";
            }
            return "Registro Inserido com sucesso";
        }
        public Integer delete(int codigo){
            Integer linhasAfetadas = bdUtil.getConexao().delete("PREFERENCIAS","_id = ?", new String[]{Integer.toString(codigo)});
            bdUtil.close();
            return linhasAfetadas;
        }

        public List<Preferencias> getAll(){
            List<Preferencias> preferencias = new ArrayList<>();
            // monta a consulta
            StringBuilder stringBuilderQuery = new StringBuilder();
            stringBuilderQuery.append("SELECT _ID, NOME, VALOR ");
            stringBuilderQuery.append("FROM  PREFERENCIAS ");
            stringBuilderQuery.append("ORDER BY NOME");
            //consulta os registros que estão no BD
            Cursor cursor = bdUtil.getConexao().rawQuery(stringBuilderQuery.toString(), null);
            //aponta cursos para o primeiro registro
            cursor.moveToFirst();
            Preferencias preferencia = null;
            //Percorre os registros até atingir o fim da lista de registros
            while (!cursor.isAfterLast()){
                // Cria objetos do tipo preferencia
                preferencia =  new Preferencias();
                //adiciona os dados no objeto
                preferencia.set_id(cursor.getInt(cursor.getColumnIndex("_ID")));
                preferencia.setNome(cursor.getString(cursor.getColumnIndex("NOME")));
                preferencia.setValor(cursor.getString(cursor.getColumnIndex("VALOR")));
                //adiciona o objeto na lista
                preferencias.add(preferencia);
                //aponta para o próximo registro
                cursor.moveToNext();
            }
            bdUtil.close();
            //retorna a lista de objetos
            return preferencias;
        }
        public Preferencias getPreferencias(String nome){
            Cursor cursor =  bdUtil.getConexao().rawQuery("SELECT * FROM preferencias WHERE nome = '"+ nome+"'",null);
            Preferencias t = new Preferencias();
            cursor.moveToFirst();

            if( cursor.getCount() > 0 ){
                t.set_id(cursor.getInt(cursor.getColumnIndex("_ID")));
                t.setNome(cursor.getString(cursor.getColumnIndex("NOME")));
                t.setValor(cursor.getString(cursor.getColumnIndex("VALOR")));
            }
            Log.d("Update", "Consulta por "+nome);
            bdUtil.close();
            return t;
        }

        public int update(Preferencias preferencia){
            ContentValues contentValues =  new ContentValues();
            contentValues.put("_ID",     preferencia.get_id());
            contentValues.put("NOME",    preferencia.getNome());
            contentValues.put("VALOR",   preferencia.getValor());
            //atualiza o objeto usando a chave
            int retorno = bdUtil.getConexao().update("PREFERENCIAS", contentValues, "_id = ?", new String[]{Integer.toString(preferencia.get_id())});
            Log.d("Update", "Atualizou "+retorno);
            bdUtil.close();
            return retorno;
        }
    }



