package com.example.soccermatch.model;

public class Presenca extends Api {
    private String idTimeAtleta;
    private String idJogo;

    public Presenca(String rota, String token, String idTimeAtleta, String idJogo) {
        super(rota, token);
        this.idTimeAtleta = idTimeAtleta;
        this.idJogo = idJogo;
    }

    public String getIdTimeAtleta() {
        return idTimeAtleta;
    }

    public void setIdTimeAtleta(String idTimeAtleta) {
        this.idTimeAtleta = idTimeAtleta;
    }

    public String getIdJogo() {
        return idJogo;
    }

    public void setIdJogo(String idJogo) {
        this.idJogo = idJogo;
    }
}
