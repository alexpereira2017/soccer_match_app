package com.example.soccermatch.services;

import android.util.Log;

import com.example.soccermatch.api.ApiData;
import com.example.soccermatch.model.AgendaJogos;
import com.example.soccermatch.model.RequisicaoRetorno;
import com.example.soccermatch.model.response.RetornoAgendaJogos;
import com.example.soccermatch.util.ApiServiceFactory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgendaService {
    RetornoAgendaJogos retorno;

    private ApiData apiService;
//    private List<AgendaJogos> listaAgendaJogos = new ArrayList<>();

    public AgendaService() {
        apiService = ApiServiceFactory.create();
    }

    public RequisicaoRetorno novo(String data, String hora) {

        AgendaJogos agendaJogos = new AgendaJogos( "NovaPartida",
                "dW5pcXVlaGFzaF9BbGV4IEwgUGVyZWlyYXwxfGFsZXhwZXJlaXJhMjAwNEBnbWFpbC5jb21lbV8xNTU3OTgyODI5"
                ,"1","1",data,hora);

        Call<RetornoAgendaJogos> call = apiService.salvarAgenda(agendaJogos);

        call.enqueue(new Callback<RetornoAgendaJogos>() {
            @Override
            public void onResponse(Call<RetornoAgendaJogos> call, Response<RetornoAgendaJogos> response) {

                if( response.isSuccessful() ){
                    Log.i("onResponse", "requisição com sucesso");
                    retorno = response.body();
//                    textoResultado.setText(
//                            "Código: " + response.code() +
//                                    " id: " + postagemResposta.getId() +
//                                    " titulo: " + postagemResposta.getTitle()
//                    );
                }

            }

            @Override
            public void onFailure(Call<RetornoAgendaJogos> call, Throwable t) {
                Log.e("onFailure","requisição falhou");
                retorno.setStatus("2");
            }
        });

        return retorno;
//        Toast.makeText(getClass(),"Hello Javatpoint",Toast.LENGTH_SHORT).show();

    }
}
