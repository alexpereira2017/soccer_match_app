package com.example.soccermatch.config;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SqliteConfig extends SQLiteOpenHelper {

    private static final int VERSAO = 2;

    public SqliteConfig(Context context) {
        super(context, "SOCCERDB", null, VERSAO);
    }

//    @Override
//    public void criar() {
//
//    }

    @Override
    public void onCreate(SQLiteDatabase bancoDados) {
        try {
            Log.d("Sqlite>", "sqliteconfig Inicializar tabelas");

            StringBuilder sql = new StringBuilder();
            sql.append("CREATE TABLE IF NOT EXISTS preferencias ");
            sql.append("    id INTEGER Primary Key autoincrement");
            sql.append("    nome VARCHAR, ");
            sql.append("    valor VARCHAR ) ");
            bancoDados.execSQL(sql.toString());

            StringBuilder insert = new StringBuilder();
            insert.append("INSERT INTO preferencias (nome, valor) VALUES (cor, 'CCCCCC')");
            bancoDados.execSQL(insert.toString());

//            Cursor cursor = bancoDados.rawQuery("SELECT nome, valor FROM preferencias", null);
//            int iNome = cursor.getColumnIndex("nome");
//            int iValor = cursor.getColumnIndex("valor");
//            cursor.moveToFirst();
            Log.d("Sqlite>", "Sucesso ao inicializar tabelas");
//
//            while (cursor != null) {
//                Log.d("Dados", cursor.getString(iNome)+" "+cursor.getString(iValor));
//            }


        } catch (Exception e) {
            Log.d("Sqlite>", "sqliteconfig Falha ao criar tabelas: "+e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("Sqlite>", "sqliteconfig onUpgrade");

    }
}
