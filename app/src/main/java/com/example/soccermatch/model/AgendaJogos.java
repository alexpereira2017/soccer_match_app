package com.example.soccermatch.model;

public class AgendaJogos extends Api {
    private String id;
    private String idTime;
    private String idLocal;
    private String dia;
    private String hora;
    private String time;
    private String local;

    public AgendaJogos(String Rota, String Token, String idTime, String idLocal, String dia, String hora) {
        super(Rota, Token);
        this.idTime = idTime;
        this.idLocal = idLocal;
        this.dia = dia;
        this.hora = hora;
    }


    @Override
    public String toString() {
        return "AgendaJogos{" +
                "id='" + id + '\'' +
                ", idTime='" + idTime + '\'' +
                ", time='" + time + '\'' +
                ", dia='" + dia + '\'' +
                ", hora='" + hora + '\'' +
                ", idLocal='" + idLocal + '\'' +
                ", local='" + local + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdTime() {
        return idTime;
    }

    public void setIdTime(String idTime) {
        this.idTime = idTime;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(String idLocal) {
        this.idLocal = idLocal;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

}
