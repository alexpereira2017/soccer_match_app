package com.example.soccermatch.model;

public class Usuario {
    protected String nome;
    protected String senha;
    protected String email;
    protected String nomeUsuario;
    protected String nomeAtleta;
    protected String idTimeAtleta;
    protected String time;

    public Usuario() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getNomeAtleta() {
        return nomeAtleta;
    }

    public void setNomeAtleta(String nomeAtleta) {
        this.nomeAtleta = nomeAtleta;
    }

    public String getIdTimeAtleta() {
        return idTimeAtleta;
    }

    public void setIdTimeAtleta(String idTimeAtleta) {
        this.idTimeAtleta = idTimeAtleta;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
